﻿class CfgPatches
{
	class rhs_hellfire_launcher_fix
	{
		units[]=
		{

		};
		ammo[]={};
		weapons[]={};
		requiredVersion=1.3200001;
		requiredAddons[]={
			"rhsusf_c_airweapons",
			"rhsusf_airweapons",
			"ace_hellfire"
		};
		magazines[]={};
	};
};
class CfgMagazines {
    class 12Rnd_PG_missiles;
    class 6Rnd_ACE_Hellfire_AGM114K: 12Rnd_PG_missiles {
    };
    class PylonRack_4Rnd_ACE_Hellfire_AGM114K: 6Rnd_ACE_Hellfire_AGM114K {
    };
    class PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114K: PylonRack_4Rnd_ACE_Hellfire_AGM114K {
        displayName = "4x AGM-114K";
        displayNameShort = "AGM-114K";
        descriptionShort = "AGM-114K";
		model = \rhsusf\addons\rhsusf_airweapons\proxypylon\rhsusf_pylon_m_agm114_4x;
    };
    class PylonRack_2Rnd_RHS_ACE_Hellfire_AGM114K: PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114K {
        displayName = "2x AGM-114K";
		model = \rhsusf\addons\rhsusf_airweapons\proxypylon\rhsusf_pylon_m_agm114_2x;
    	count = 2;
		mirrorMissilesIndexes[]	= {2,1};
		hardpoints[] = {"RHS_HP_MELB"};
    };
    class PylonRack_4Rnd_ACE_Hellfire_AGM114N: PylonRack_4Rnd_ACE_Hellfire_AGM114K {
    };
    class PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114N: PylonRack_4Rnd_ACE_Hellfire_AGM114N {
        displayName = "4x AGM-114N";
        displayNameShort = "AGM-114N";
        descriptionShort = "AGM-114N";
		model = \rhsusf\addons\rhsusf_airweapons\proxypylon\rhsusf_pylon_m_agm114_4x;
    };
    class PylonRack_2Rnd_RHS_ACE_Hellfire_AGM114N: PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114N {
        displayName = "2x AGM-114N";
		model = \rhsusf\addons\rhsusf_airweapons\proxypylon\rhsusf_pylon_m_agm114_2x;
    	count = 2;
		mirrorMissilesIndexes[]	= {2,1};
		hardpoints[] = {"RHS_HP_MELB"};
    };
    class PylonRack_4Rnd_ACE_Hellfire_AGM114L: PylonRack_4Rnd_ACE_Hellfire_AGM114K {
    };
    class PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114L: PylonRack_4Rnd_ACE_Hellfire_AGM114L {
        displayName = "4x AGM-114L";
        displayNameShort = "AGM-114L";
        descriptionShort = "AGM-114L";
    };
};
class CfgWeapons {
	class RocketPods;
	class ace_hellfire_launcher: RocketPods {
        magazines[] = {"6Rnd_ACE_Hellfire_AGM114K", "PylonMissile_1Rnd_ACE_Hellfire_AGM114K", "PylonRack_1Rnd_ACE_Hellfire_AGM114K", "PylonRack_3Rnd_ACE_Hellfire_AGM114K", "PylonRack_4Rnd_ACE_Hellfire_AGM114K", "PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114K", "PylonRack_2Rnd_RHS_ACE_Hellfire_AGM114K"};
	}
	class ace_hellfire_launcher_N: ace_hellfire_launcher {
        magazines[] = {"6Rnd_ACE_Hellfire_AGM114N", "PylonMissile_1Rnd_ACE_Hellfire_AGM114N", "PylonRack_1Rnd_ACE_Hellfire_AGM114N", "PylonRack_3Rnd_ACE_Hellfire_AGM114N", "PylonRack_4Rnd_ACE_Hellfire_AGM114N", "PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114N", "PylonRack_2Rnd_RHS_ACE_Hellfire_AGM114N"};
	};
	class ace_hellfire_launcher_L: ace_hellfire_launcher {
        magazines[] = {"6Rnd_ACE_Hellfire_AGM114L", "PylonMissile_1Rnd_ACE_Hellfire_AGM114L", "PylonRack_1Rnd_ACE_Hellfire_AGM114L", "PylonRack_3Rnd_ACE_Hellfire_AGM114L", "PylonRack_4Rnd_ACE_Hellfire_AGM114L", "PylonRack_4Rnd_RHS_ACE_Hellfire_AGM114L"};
	};
};
class cfgMods
{
	author="Watt";
	timepacked="1509475975";
};