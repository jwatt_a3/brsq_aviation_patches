﻿class CfgPatches
{
	class agm114_model_fix
	{
		units[]=
		{

		};
		ammo[]={};
		weapons[]={};
		requiredVersion=1.3200001;
		requiredAddons[]={
			"rhsusf_airweapons",
			"ace_hellfire"
		};
		magazines[]={};
	};
};
class CfgAmmo {
    class M_Scalpel_AT;
    // Change ACE Hellfire to use RHS Hellfire model
    class ACE_Hellfire_AGM114K: M_Scalpel_AT {
		model="\rhsusf\addons\rhsusf_airweapons\proxyammo\rhsusf_m_AGM114K_fly";
		proxyShape="\rhsusf\addons\rhsusf_airweapons\proxyammo\rhsusf_m_AGM114K";
    };
    class ACE_Hellfire_AGM114N: ACE_Hellfire_AGM114K {
		model="\rhsusf\addons\rhsusf_airweapons\proxyammo\rhsusf_m_AGM114K_fly";
		proxyShape="\rhsusf\addons\rhsusf_airweapons\proxyammo\rhsusf_m_AGM114K";
    };
    class ACE_Hellfire_AGM114L: ACE_Hellfire_AGM114K {
		model="\rhsusf\addons\rhsusf_airweapons\proxyammo\rhsusf_m_AGM114K_fly";
		proxyShape="\rhsusf\addons\rhsusf_airweapons\proxyammo\rhsusf_m_AGM114K";
	};
};

class cfgMods
{
	author="Watt";
	timepacked="1509475975";
};